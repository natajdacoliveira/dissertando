import { UserFirestore } from '@/components/models';
import { auth, db } from 'boot/firebase';
import { FirebaseError } from 'firebase/app';
import {
  EmailAuthProvider,
  GoogleAuthProvider,
  SignInMethod,
  User,
  createUserWithEmailAndPassword,
  fetchSignInMethodsForEmail,
  isSignInWithEmailLink,
  linkWithPopup,
  sendEmailVerification,
  sendSignInLinkToEmail,
  signInWithEmailAndPassword,
  signInWithEmailLink,
  signInWithPopup,
  signOut,
} from 'firebase/auth';
import { Timestamp, doc, getDoc, setDoc } from 'firebase/firestore';
import { defineStore } from 'pinia';
import { Notify, QNotifyOptions } from 'quasar';
import { ErrorHandler, GetCustomErrorMessage, Redirect } from 'src/utils';

const notifySuccessCreateOptions: QNotifyOptions = {
  timeout: 500,
  type: 'positive',
};
const notifyOnGoingOptions: QNotifyOptions = {
  type: 'ongoing',
};

const saveUserData = async (user: User): Promise<void> => {
  let linkedWithPassword = false;
  let linkedWithGoogle = false;
  let linkedWithEmailLink = false;
  if (!user.email) return;

  const [signInMethods] = await ErrorHandler(
    fetchSignInMethodsForEmail(auth, user.email)
  );
  if (
    signInMethods?.indexOf(SignInMethod.EMAIL_LINK) !== -1 &&
    signInMethods?.length === 1
  ) {
    // Only emailLink users
    linkedWithEmailLink = true;
  }
  if (signInMethods?.indexOf(SignInMethod.EMAIL_PASSWORD) !== -1) {
    linkedWithPassword = true;
  }
  if (signInMethods?.indexOf(SignInMethod.GOOGLE) !== -1) {
    linkedWithGoogle = true;
  }

  const data: UserFirestore = {
    createdAt: Timestamp.now(),
    displayName: user.displayName,
    email: user.email,
    emailVerified: user.emailVerified,
    lastSeenAt: Timestamp.now(),
    linkedWithEmailLink,
    linkedWithGoogle,
    linkedWithPassword,
    onlineStatus: true,
    phoneNumber: user.phoneNumber,
    photoURL: user.photoURL,
    providerId: user.providerId,
    uid: user.uid,
    updateAt: Timestamp.now(),
  };
  const docRef = doc(db, 'user', user.uid);
  return setDoc(docRef, data);
};

const sendEmailLink = async (email: string) => {
  const actionCodeSettings = {
    dynamicLinkDomain: 'dissertando.page.link',
    handleCodeInApp: true,
    url: 'http://localhost:9000/usuario/entrar?signInWithEmailLink=true',
  };
  localStorage.setItem('emailForSignIn', email);
  return await sendSignInLinkToEmail(auth, email, actionCodeSettings);
};

export const userStore = defineStore('user', {
  actions: {
    async createUserWithEmailAndPassword(
      user: string,
      password: string
    ): Promise<boolean> {
      const loadingNotification = Notify.create({
        message: 'Realizando o cadastro...',
        ...notifyOnGoingOptions,
      });
      const [userCredential, error] = await ErrorHandler(
        createUserWithEmailAndPassword(auth, user, password)
      );
      if (error) {
        loadingNotification({
          message: GetCustomErrorMessage(error),
          timeout: 1000,
          type: 'warn',
        });
        return false;
      }
      if (!userCredential) return false;

      this.$state.user = userCredential.user;
      await ErrorHandler(saveUserData(userCredential.user));
      loadingNotification({
        message: 'Cadastro realizado com sucesso.',
        timeout: 500,
        type: 'positive',
      });
      await ErrorHandler(Redirect(this.router));
      return true;
    },

    async getUserFromFirestore() {
      if (!this.user) return null;
      const userRef = doc(db, 'user', this.user?.uid);
      const [user] = await ErrorHandler(getDoc(userRef));
      this.userFirestoreData = <UserFirestore>user?.data();
    },

    handleDisconnect() {
      // Handles disconnect user token expiration
      this.user = null;
      this.userFirestoreData = null;
      this.router.push({ name: 'user/sign-in' });
    },

    async handleSignIn(
      email: string
    ): Promise<'password' | 'emailLink' | undefined> {
      const [signInMethods] = await ErrorHandler(
        fetchSignInMethodsForEmail(auth, email)
      );
      if (
        signInMethods &&
        signInMethods.indexOf(
          EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD
        ) !== -1
      ) {
        return EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD;
      }
      const [, error] = await ErrorHandler(sendEmailLink(email));
      if (error) {
        return EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD;
      }
      return EmailAuthProvider.EMAIL_LINK_SIGN_IN_METHOD;
    },

    async linkAccountWithGoogle() {
      if (!auth.currentUser) return null;
      const googleProvider = new GoogleAuthProvider();
      const [userCredential] = await ErrorHandler(
        linkWithPopup(auth.currentUser, googleProvider)
      );
      if (!userCredential) return;

      this.$state.user = userCredential.user;
      await ErrorHandler(saveUserData(userCredential.user));

      Notify.create({
        message: 'Conexão realizada com sucesso!',
        ...notifySuccessCreateOptions,
      });
    },

    async sendEmailLink(email: string) {
      const [, error] = await ErrorHandler(sendEmailLink(email));
      return error;
    },

    async sendEmailVerification() {
      if (!this.user) return null;
      const [user] = await ErrorHandler(sendEmailVerification(this.user));
      console.log(user);
    },

    async signInWithEmailAndPassword(user: string, password: string) {
      const loadingNotification = Notify.create({
        message: 'Realizando o login...',
        ...notifyOnGoingOptions,
      });
      const [userCredential, error] = await ErrorHandler(
        signInWithEmailAndPassword(auth, user, password)
      );
      if (error) {
        loadingNotification({
          message: GetCustomErrorMessage(error),
          timeout: 1000,
          type: 'warn',
        });
      }

      if (userCredential) {
        this.$state.user = userCredential.user;
        await ErrorHandler(ErrorHandler(saveUserData(userCredential.user)));

        loadingNotification({
          message: 'Login realizado com sucesso.',
          timeout: 500,
          type: 'positive',
        });
        await ErrorHandler(Redirect(this.router));
      }
    },

    async signInWithEmailLink(email: string, emailLink: string) {
      const loadingNotification = Notify.create({
        message: 'Realizando o login...',
        ...notifyOnGoingOptions,
      });

      if (!isSignInWithEmailLink(auth, emailLink))
        throw new FirebaseError('auth/invalid-email', '');

      const [userCredential] = await ErrorHandler(
        signInWithEmailLink(auth, email, emailLink)
      );
      if (!userCredential) return loadingNotification();
      this.$state.user = userCredential.user;
      await ErrorHandler(saveUserData(userCredential.user));

      // TODO this message here should not always be cadastro, but adapt if user has already logged in with other methods
      // very annyoing
      loadingNotification({
        message: 'Cadastro realizado com sucesso.',
        timeout: 500,
        type: 'positive',
      });
      this.$state.user = userCredential.user;
      await ErrorHandler(saveUserData(userCredential.user));
      await ErrorHandler(Redirect(this.router));
    },

    async signInWithGoogle() {
      const provider = new GoogleAuthProvider();
      const [userCredential] = await ErrorHandler(
        signInWithPopup(auth, provider)
      );
      if (!userCredential) return;
      this.$state.user = userCredential.user;
      await ErrorHandler(saveUserData(userCredential.user));
      Notify.create({
        message: 'Login realizado com sucesso.',
        position: 'top',
        timeout: 500,
        type: 'positive',
      });
      await ErrorHandler(Redirect(this.router));
    },

    async signOut() {
      await ErrorHandler(signOut(auth));
    },
  },
  getters: {},
  state: () => ({
    isEmailLinkHref: false as boolean,
    user: null as User | null,
    userFirestoreData: null as UserFirestore | null,
  }),
});
