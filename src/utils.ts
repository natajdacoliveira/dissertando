import { AxiosError } from 'axios';
import { FirebaseError } from 'firebase/app';
import { Router } from 'vue-router';

export const GetCustomErrorMessage = (error: unknown): string | undefined => {
  console.log(error);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  console.log((<any>error).code || 'no code');
  const errorMessages: Record<string, string> = {
    'auth/credential-already-in-use':
      'Essa conta já é conectada à uma conta Google.',
    'auth/email-already-in-use': 'Este e-mail já está cadastrado.',
    'auth/expired-action-code': 'Este link mágico expirou.',
    'auth/internal-error': 'Erro interno, por favor tente mais tarde.',
    'auth/invalid-action-code': 'Este link mágico é inválido.',
    'auth/invalid-email': 'Esse e-mail não é válido.',
    'auth/quota-exceeded':
      'A quota de links mágicos foi excedida. Por favor, utilize outro provedor de login',
    'auth/user-disabled': 'Esse usuário foi desabilitado.',
    'auth/user-not-found': 'Erro ao realizar o login.',
    'auth/weak-password': 'Essa senha não é válida: * No mínimo 6 caracteres.',
    'auth/wrong-password': 'Erro ao realizar o login.',
    'permission-denied': 'Acesso negado, verifique suas permissões.',
  };

  if (error instanceof FirebaseError) {
    // TODO:
    // case 'auth/email-already-in-use':
    // Enviar confirmação de e-mail para usuário. Recuperar conta
    // case 'auth/popup-closed-by-user':
    // case 'auth/cancelled-popup-request':
    // Can't check when popup was closed/cancelled -- See if there's a way for it
    return errorMessages[error.code] || undefined;
  } else if (error instanceof AxiosError) {
    return error.response ? error.response.data.message : String(error);
  } else if (error instanceof Error) {
    return error.message;
  }
  return undefined;
};

export const ErrorHandler = async <T>(
  promise: Promise<T>
): Promise<[T | null, unknown]> => {
  try {
    return [await promise, null];
  } catch (error) {
    return [null, error];
  }
};

export const Redirect = (router: Router) => {
  const redirectedFrom =
    (typeof router.currentRoute.value.redirectedFrom?.name === 'string'
      ? router.currentRoute.value.redirectedFrom?.name
      : 'dashboard') ||
    (typeof router.currentRoute.value.query?.redirect === 'string'
      ? router.currentRoute.value.query.redirect
      : 'dashboard') ||
    'dashboard';

  return router.push({
    name: redirectedFrom,
  });
};

export const IsEmailValid = (email: string) => {
  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    email
  );
};

export const GetInputVisibilityIconPassword = (
  isPasswordVisible: boolean
): 'visibility' | 'visibility_off' => {
  return isPasswordVisible ? 'visibility' : 'visibility_off';
};

export const GetInputTypePassword = (
  isPasswordVisible: boolean
): 'text' | 'password' => {
  return isPasswordVisible ? 'text' : 'password';
};
