import { QNotifyOptions } from 'quasar';

export type WithRequired<T, K extends keyof T> = T & { [P in K]-?: T[P] };
export type QNotifyWithRequiredTypeAndTimeout = WithRequired<
  QNotifyOptions,
  'type' | 'timeout'
>;
