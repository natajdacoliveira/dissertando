import { route } from 'quasar/wrappers';
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory,
} from 'vue-router';

import routes from './routes';
import { userStore } from 'stores/user';
import { auth } from 'boot/firebase';
import { User, onAuthStateChanged } from 'firebase/auth';
// import { authStore } from 'stores/auth';

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(({}) => {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === 'history'
    ? createWebHistory
    : createWebHashHistory;

  const Router = createRouter({
    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.VUE_ROUTER_BASE),
    routes,
    scrollBehavior: () => ({ left: 0, top: 0 }),
  });

  const getCurrentUser = () => {
    return new Promise<void>((resolve, reject) => {
      const removeListener = onAuthStateChanged(
        auth,
        async (user: User | null) => {
          removeListener();
          const userStoreInstance = userStore();
          userStoreInstance.user = user;
          await userStoreInstance.getUserFromFirestore();
          resolve();
        },
        reject
      );
    });
  };

  Router.beforeEach(async (to) => {
    const userStoreInstance = userStore();
    await getCurrentUser();
    const openRoutesBeforeLogin: string[] = [
      'user/sign-in',
      'user/sign-up',
      'user/recover-password',
    ];

    const closedRoutesAfterLogin: string[] = ['user/sign-in', 'user/sign-up'];
    if (
      !openRoutesBeforeLogin.includes(to.name?.toString() || '') &&
      !userStoreInstance.user
    )
      return {
        name: 'user/sign-in',
        query: {
          redirect: to.name && typeof to.name === 'string' ? to.name : '',
        },
      };
    else if (
      userStoreInstance.user &&
      closedRoutesAfterLogin.includes(to.name?.toString() || '')
    ) {
      const redirectedFrom: string =
        (typeof to.redirectedFrom?.name === 'string'
          ? to.redirectedFrom?.name
          : 'dashboard') ||
        (typeof to.query?.redirect === 'string'
          ? to.query.redirect
          : 'dashboard') ||
        'dashboard';
      return {
        name: redirectedFrom,
      };
    }
  });
  return Router;
});
