import { RouteRecordRaw } from 'vue-router';

// The paths of imports and 'path' itself need to be in perfect CASE MATCHING
// otherwise you will experience almost physical pain...
const MainLayout = () => import('layouts/MainLayout.vue');
const UserLayout = () => import('layouts/UserLayout.vue');
const SignIn = () => import('pages/SignIn.vue');
const SignUpPage = () => import('pages/SignUpPage.vue');
const RecoverPasswordPage = () => import('pages/RecoverPasswordPage.vue');
const ErrorNotFound = () => import('pages/ErrorNotFound.vue');
const DashboardPage = () => import('pages/DashboardPage.vue');

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: { name: 'dashboard' },
  },

  {
    children: [{ component: DashboardPage, name: 'dashboard', path: '' }],
    component: MainLayout,
    path: '/painel',
  },

  // TODO Take user email
  // Either log in with google/other providers
  // Or redirect to email sign in then save password aswell
  {
    children: [
      {
        component: SignIn,
        name: 'user/sign-in',
        path: 'entrar',
      },
      {
        component: SignUpPage,
        name: 'user/sign-up',
        path: 'registrar',
      },
      {
        component: RecoverPasswordPage,
        name: 'user/recover-password',
        path: 'recuperar-senha',
      },
    ],
    component: UserLayout,
    name: 'user',

    path: '/usuario',
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    component: ErrorNotFound,
    path: '/:catchAll(.*)*',
  },
];

export default routes;
