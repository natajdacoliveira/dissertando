import { boot } from 'quasar/wrappers';
// Import the functions you need from the SDKs you need
import { FirebaseOptions, initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';
import { userStore } from 'stores/user';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebase = initializeApp(process.env.FIREBASE_CONFIG as FirebaseOptions);

const auth = getAuth(firebase);
const db = getFirestore(firebase);
let firstLoad = true;

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(({}) => {
  auth.onIdTokenChanged(() => {
    if (!auth.currentUser && !firstLoad) {
      const userStoreInstance = userStore();
      userStoreInstance.handleDisconnect();
    }
    if (firstLoad) firstLoad = false;
  });
});

export { firebase, auth, db };
