import { Timestamp } from 'firebase/firestore';

export interface Todo {
  content: string;
  id: number;
}

export interface Meta {
  totalCount: number;
}

export interface UserFirestore {
  createdAt: Timestamp;
  displayName: string | null;
  email: string | null;
  emailVerified: boolean;
  lastSeenAt: Timestamp;
  linkedWithEmailLink: boolean;
  linkedWithGoogle: boolean;
  linkedWithPassword: boolean;
  onlineStatus: boolean;
  phoneNumber: string | null;
  photoURL: string | null;
  providerId: string;
  uid: string;
  updateAt: Timestamp;
}

export interface AlternativeLogin {
  alt: string;
  click: () => void;
  name: string;
  src: string;
  type?: 'img' | 'icon';
}
